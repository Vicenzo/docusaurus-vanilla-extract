import { style } from '@vanilla-extract/css';

export const heroBanner = style({
  padding: '4rem 0',
  textAlign: 'center',
  position: 'relative',
  overflow: 'hidden',
  backgroundColor: '#2E8555',
  color: '#FFF'
})

export const button = style({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
})

